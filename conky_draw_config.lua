-- Define your visual elements here
-- For examples, and a complete list on the possible elements and their 
-- properties, go to https://github.com/fisadev/conky-draw/
-- (and be sure to use the lastest version)
--[[
elements = {
    {   kind = 'bar_graph',
        conky_value = 'cpu cpu0',
        from = {x = cpu_x, y = cpu_y},
        to = {x = cpu_x+, y = cpu_y},
        background_thickness = 20,
        bar_thickness = 16,
        
        graduated = true,
        number_graduation = 30,
        space_between_graduation = 2,

        
    },
}
--]]
function generateElements()
    e = {}
    -- cpu bars    
    local cpu_x = 40
    local cpu_y = 145
    local cpu_height = 110
    local thickness = 67
    for i = 0,11 do
        this_x = cpu_x  + i * thickness * 1.09
        table.insert(e, {
            kind = 'bar_graph',
            conky_value = 'cpu cpu'..i,
            critical_threshold = 90,
            max_value = 100,
            bar_color_critical = 0xFF4500,--0xFF7F50,
            from = {x = this_x, y = cpu_y},
            to = {x = this_x, y = cpu_y - cpu_height},
            background_thickness = thickness,
            bar_thickness = thickness * 0.95,
            
            graduated = true,
            number_graduation = 18,
            space_between_graduation = 1.5,         
        })
        table.insert(e, {
            kind = 'variable_text',
            conky_value = 'freq '..i,
            from = {x = this_x - 27, y = cpu_y + 16},
            bold = true,
        })
        table.insert(e, {
            kind = 'static_text',
            text = 'MHz',
            bold = true,
            from = {x = this_x + 1, y = cpu_y + 16},
        })
    end

    

    -- cpu ellipse
    table.insert(e,{
        kind = 'ellipse_graph',
        center = {x = 950, y = 85},
        conky_value = 'cpu',
        --max_value = 100,
        width = 75,
        height = 75,
        --bar_color = 0xFF6600,
        --background_color=0xFF6600,
        start_angle = 180,
        end_angle = 540,
        bar_thickness = 24,
        background_thickness = 24,
        
    })

    --cpu temp
    --[[table.insert(e, {
        kind = 'bar_graph',
        conky_value = "execp sensors | grep temp1 | tr -s ' '| cut -d' ' -f2 | sed 's/[^0-9/.]*//g'",
        from = {x = 1100, y = 150},
        to = {x=1100, y = 35},
        max_value = 100,
        critical_threshold = 70,
        background_thickness = thickness,
        bar_thickness = thickness * 0.95,
        
    })--]]

    table.insert(e, {
            kind = 'variable_text',
            conky_value = "execp sensors | grep k10 -A 2 | grep temp1 | tr -s ' ' | cut -d' ' -f2", --| sed 's/[^0-9/.]*//g'",
            from = {x = 913, y = 160},
            bold = true,
            font_size = 20,
            alpha = .5,
        })    

    -- ram bar
    --[[local ram_x = cpu_x
    local ram_y = 180
    local ram_width = cpu_width
    local ram_thickness = thickness * 0.8
    table.insert(e,{
        kind = 'bar_graph',
        conky_value = 'memperc',
        from = {x = ram_x, y=ram_y},
        to = {x = ram_x + ram_width, y = ram_y},
        bar_thickness = ram_thickness * 0.8,
        background_thickness = ram_thickness,
    })

    -- swap bar
    local swap_y = 215
    table.insert(e,{
        kind = 'bar_graph',
        conky_value = 'swapperc',
        from = {x = ram_x, y = swap_y},
        to = {x = ram_x + ram_width, y = swap_y},
        bar_thickness = ram_thickness * 0.8,
        background_thickness = ram_thickness,
    })--]]
    return e
end
